#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSString *astring;
        astring = [NSString stringWithCString:"This is a temporary string"];
        NSLog(@"astring:%@",astring);
        
        /*----------------从文件读取字符串:initWithContentsOfFile方法----------------*/
        NSString *path = @"astring.text";
        NSString *astring1 = [[NSString alloc] initWithContentsOfFile:path];
        NSLog(@"astring1:%@",astring1);
        [astring1 release];
        
        /*----------------写字符串到文件:writeToFile方法----------------*/
        NSString *astring2 = [[NSString alloc] initWithString:@"This is a String!"];
        NSLog(@"astring2:%@",astring);
        NSString *path1 = @"astring.text";
        [astring2 writeToFile: path1 atomically: YES];
        [astring2 release];
        /*----------------比较两个字符串----------------*/
        //用C比较:strcmp函数
        
        char string1[] = "string!";
        char string2[] = "string!";
        if(strcmp(string1, string2) == 0)
        {
            NSLog(@"1");
        }
        
        
        //isEqualToString方法
        NSString *astring01 = @"This is a String!";
        NSString *astring02 = @"This is a String!";
        BOOL result = [astring01 isEqualToString:astring02];
        NSLog(@"result:%d",result);
        
        
        //compare方法(comparer返回的三种值)
        BOOL result1 = [astring01 compare:astring02] == NSOrderedSame;
        NSLog(@"result1:%d",result1);
        //NSOrderedSame判断两者内容是否相同
        
        
        BOOL result2 = [astring01 compare:astring02] == NSOrderedAscending;
        NSLog(@"result2:%d",result2);
        //NSOrderedAscending判断两对象值的大小(按字母顺序进行比较，astring02大于astring01为真)
        
        
        BOOL result3 = [astring01 compare:astring02] == NSOrderedDescending;
        NSLog(@"result3:%d",result3);
        //NSOrderedDescending判断两对象值的大小(按字母顺序进行比较，astring02小于astring01为真)
        
        
        //不考虑大小写比较字符串1
        BOOL result4 = [astring01 caseInsensitiveCompare:astring02] == NSOrderedSame;
        NSLog(@"result4:%d",result4);
        //NSOrderedDescending判断两对象值的大小(按字母顺序进行比较，astring02小于astring01为真)
        
        
        //不考虑大小写比较字符串2
        BOOL result5 = [astring01 compare:astring02
                                 options:NSCaseInsensitiveSearch | NSNumericSearch] == NSOrderedSame;
        NSLog(@"result5:%d",result5);
        
        //NSCaseInsensitiveSearch:不区分大小写比较 NSLiteralSearch:进行完全比较，区分大小写 NSNumericSearch:比较字符串的字符个数，而不是字符值。
        
        
        /*----------------改变字符串的大小写----------------*/
        
        NSString *string03 = @"A String";
        NSString *string04 = @"String";
        NSLog(@"string03:%@",[string03 uppercaseString]);//大写
        NSLog(@"string04:%@",[string04 lowercaseString]);//小写
        NSLog(@"string04:%@",[string04 capitalizedString]);//首字母大小
        
        
        /*----------------在串中搜索子串----------------*/
        
        NSString *string05 = @"This is a string";
        NSString *string06 = @"string";
        NSRange range = [string05 rangeOfString:string06];
        int location = range.location;
        int leight = range.length;
        NSString *astring04 = [[NSString alloc] initWithString:[NSString stringWithFormat:@"Location:%i,Leight:%i",location,leight]];
        NSLog(@"astring04:%@",astring04);
        [astring release];
        
        
        /*----------------抽取子串 ----------------*/
        
        //-substringToIndex: 从字符串的开头一直截取到指定的位置，但不包括该位置的字符
        NSString *string07 = @"This is a string";
        NSString *string08 = [string07 substringToIndex:3];
        NSLog(@"string08:%@",string08);
        
        
        //-substringFromIndex: 以指定位置开始（包括指定位置的字符），并包括之后的全部字符
        NSString *string09 = [string07 substringFromIndex:3];
        NSLog(@"string09:%@",string09);
        
        
        //-substringWithRange: //按照所给出的位置，长度，任意地从字符串中截取子串
        NSString *string10 = [string07 substringWithRange:NSMakeRange(0, 4)];
        NSLog(@"string10:%@",string10);
        
        
        //扩展路径
        
        NSString *Path = @"~/NSData.txt";
        NSString *absolutePath = [Path stringByExpandingTildeInPath];
        NSLog(@"absolutePath:%@",absolutePath);
        NSLog(@"Path:%@",[absolutePath stringByAbbreviatingWithTildeInPath]);
        
        
        
        //文件扩展名
        NSString *Path5 = @"~/NSData.txt";
        NSLog(@"Extension:%@",[Path5 pathExtension]);
    }
    return 0;
}
